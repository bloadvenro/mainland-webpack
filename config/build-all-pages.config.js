const { readdirSync } = require('fs');
const { resolve } = require('path');

const PAGES_CONFIG_DIR = resolve(__dirname, 'pages');

const restorePathname = filename => `${PAGES_CONFIG_DIR}/${filename}`;

const initialize = env => config => config(env);

module.exports = env =>
  readdirSync(PAGES_CONFIG_DIR)
    .map(restorePathname)
    .map(require)
    .map(initialize(env));
