const { resolve } = require('path');
const { src, kebabize: _ } = require('../common/helpers');
const commonConfig = require('../common/common.config');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

function localConfig() {
  return {
    context: src('fjwwed'),
    entry: {
      ['v1']: './v1',
      ['v2']: './v2',
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'v1.pug',
        filename: 'fjwwed-v1.html',
        excludeChunks: ['v2'],
      }),
      new HtmlWebpackPlugin({
        template: 'v2.pug',
        filename: 'fjwwed-v2.html',
        excludeChunks: ['v1'],
      }),
    ],
  };
}

module.exports = env => merge(commonConfig(env), localConfig(env));
