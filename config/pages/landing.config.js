const { src, kebabize: _ } = require('../common/helpers');
const commonConfig = require('../common/common.config');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

function localConfig() {
  return {
    context: src('landing'),
    entry: {
      ['v1-head']: './v1-head',
      ['v1-body']: './v1-body',
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'v1.pug',
        filename: 'landing.html',
        inject: false,
        _headChunk: 'v1-head',
        _bodyChunk: 'v1-body',
      }),
    ],
  };
}

module.exports = env => merge(commonConfig(env), localConfig(env));
