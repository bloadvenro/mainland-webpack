const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { isDevelopment, isProduction, root, dist } = require('./helpers');

module.exports = ({ mode }) => ({
  mode,
  module: {
    rules: [
      {
        oneOf: [
          /**
           * SASS LOADER
           *
           * Though we don't actively use sass preprocessor it is better to have it for the future.
           * All style entries are written have *.scss extensions.
           */
          {
            test: /\.scss$/,
            use: [
              MiniCssExtractPlugin.loader,
              'css-loader',
              'resolve-url-loader',
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true,
                  sourceMapContents: false,
                },
              },
            ],
          },
          /**
           * CSS LOADER
           *
           * This loader plays in assistance with sass loader. It's only purpose is to import raw
           * *.css files inside *.scss (no need to rename all *.css files to *.scss).
           */
          {
            test: /\.css$/,
            use: ['css-loader'],
          },
          /**
           * IMAGES PREPROCESSING LOADER
           *
           * The first loader, url-loader, automatically converts all small image files into inline
           * base64. We can set image size with `limit` option.
           *
           * If the `limit` is exceeded, fallback loader will handle images. We now use file-loader
           * to just copy manually minified images to the destination folder. In the future we have
           * to replace file-loader with https://github.com/tcoopman/image-webpack-loader#readme.
           */
          {
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [
              {
                loader: 'url-loader',
                options: {
                  limit: 8192,
                  fallback: {
                    loader: 'file-loader',
                    options: {
                      name: isProduction(mode) ? '[path][hash].[ext]' : '[path][name].[ext]',
                    },
                  },
                },
              },
            ],
          },
          {
            test: /\.(ttf|eot|woff|woff2)$/,
            use: {
              loader: 'file-loader',
              options: {
                name: 'fonts/[name].[ext]',
              },
            },
          },
          {
            test: /\.pug$/,
            use: ['pug-loader'],
          },
          /**
           *
           * FALLBACK LOADER
           *
           * Would handle all file extensions that weren't caught by above rules.
           *
           * Warning: we have to ignore all js files in test case because webpack handles them by
           * default!
           */
          {
            test: filename => (/\.js$/.test(filename) ? false : true),
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: isProduction(mode) ? '[path][hash].[ext]' : '[path][name].[ext]',
                },
              },
            ],
          },
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin([dist()], {
      root: root(),
      // Cleaning will fire many times in multicompiler mode but that's ok. Just turn the log off.
      verbose: false,
    }),
    new MiniCssExtractPlugin({
      filename: isProduction(mode) ? 'styles/[name].[hash].css' : 'styles/[name].css',
    }),
    isDevelopment(mode) &&
      new BrowserSyncPlugin({
        host: 'localhost',
        port: 3000,
        server: { baseDir: [dist()] },
        directory: true,
      }),
  ].filter(Boolean),
  devtool: isProduction(mode) ? false : 'eval',
  optimization: optimization(isProduction(mode)),
  output: {
    filename: isProduction(mode) ? 'scripts/[name].[hash].js' : 'scripts/[name].bundle.js',
    path: dist(),
    publicPath: isProduction(mode) // `publicPath` MUST be set in any project's configuration file!
      ? '/remastered/' // https://www.jackpotsecrets.net/>>remastered<</*.html
      : '/', // for local development (NEVER USE empty string!!)
  },
});

function optimization(isProduction) {
  let optimization = {};

  if (isProduction) {
    optimization = {
      minimizer: [
        new UglifyJsPlugin({
          cache: false,
          parallel: true,
          sourceMap: false,
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
    };
  }

  return optimization;
}
