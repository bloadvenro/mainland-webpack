const { resolve } = require('path');

const __rootdirname = resolve(__dirname, '..', '..');

const root = (...path) => resolve(__rootdirname, ...path);
const src = (...path) => root('src', 'pages', ...path);
const dist = (...path) => root('dist', ...path);

const isProduction = mode => mode === 'production';
const isDevelopment = mode => mode === 'development';

module.exports = {
  root,
  src,
  dist,
  isProduction,
  isDevelopment,
};
