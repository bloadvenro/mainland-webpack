_It it recommended to use nodejs version 10.x.x_

# Steps to start working on the project

1. clone webpack repo

```
git clone https://gitlab.com/bloadvenro/mainland-webpack.git
```

2. cd and install deps

```
cd mainland-webpack && yarn
```

3. clone pages repo into the `src` directory

```
git clone https://gitlab.com/mainalnddigital/tribune.git src
```

Now open the `src` directory inside your editor to work on pages. To add some changes switch to or
create branch for specific page, then merge commits into `master` branch. No need to merge
`master` commits back to page-specific branch. Also push all new page-specific branches to the repo
to let the others continue making changes after you in the future.

Do not use `development` branch (it's will be deprecated).

# Launching webpack

You will find some scripts inside `scripts` directory in `mainland/webpack` repo folder. Execute
each like `sh ./scripts/<script>.sh`
